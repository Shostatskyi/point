#include "Point.h"
#include <iostream>
#include <cmath>

#define toDeg    180.0 / 3.141593
#define fromDeg  3.141593 / 180.0
 
double Point::distance(Point const &a, const Point &b)
{   
	return sqrt(pow(b.get_cart().x - a.get_cart().x, 2) + pow(b.get_cart().y - a.get_cart().y, 2));
}

void Point::toPolarCoor() 
{
	_polar.r = sqrt(_cart.x  * _cart.x + _cart.y * _cart.y);

	_polar.t = atan(_cart.y / _cart.x) * toDeg;
}

void Point::toCartesianCoor()
{
	_cart.x = _polar.r * cos(_polar.t * fromDeg);

	_cart.y = _polar.r * sin(_polar.t * fromDeg);
}

void Point::show_polar()
{
	this->toPolarCoor();
	std::cout << "r = " << _polar.r << ", t = " << _polar.t << ' ';
}

void Point::show_cartesian()  
{ 
	this->toCartesianCoor();
	std::cout << "x = " << _cart.x << ", y = " << _cart.y << ' '; 
}
