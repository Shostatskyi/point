#include "Point.h"
#include "Space.h"
#include <iostream>
using namespace std;

void main() 
{
	Point example(12, 5), example2(8, 2);
	example.show_polar();
	example.toCartesianCoor();
	example.show_cartesian();
	cout << endl << Point::distance(example, example2)  << endl;

	Space sp_exple1(2, 5, 8), sp_exple2(2, 5, 8);
	sp_exple1.show_space_point();
	std::cout << Space::distance(sp_exple1, sp_exple2) << endl;

	system("pause");
}