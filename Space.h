#pragma once
#include "Point.h"
#include <iostream>

class Space : public Point{
private:
	double _z;
public:
	Space()                             : Point(0, 0), _z(0) {}
	Space(double x, double y, double z) : Point(x, y), _z(z) {}
	Space(const Point &x_y)             : Point(x_y.get_cart().x, x_y.get_cart().y), _z(0) {}
	Space(const Point &x_y, double z)   : Point(x_y.get_cart().x, x_y.get_cart().y), _z(z) {}
	Space(const Space &spc)             : Point(spc.get_cart().x, spc.get_cart().y), _z(spc._z) {}

	void show_space_point() { this->show_cartesian(), std::cout << ", z = " << _z << "\n"; }
	double dsitance(Space const &a, Space const &b);
};
