﻿#pragma once
#include <iostream> 
using namespace std;

class Point {
private:
	struct Cart {
		double x;
		double y;
	} _cart;
	
	struct Polar {
		double r;
		double t; 
	} _polar;
	
public:

	Point() 
	{
		_cart.x  = 0, _cart.y  = 0, 
		_polar.r = 0, _polar.t = 0;
	}

	Point(double x, double y) 
	{
		_cart.x = x, _cart.y = y;
		this->toPolarCoor();
	}

	Point(Point const &p) 
	{
		_cart.x = p._cart.x, _cart.y = p._cart.y;
		_polar.r = p._polar.r, _polar.t = p._polar.t;
	}

	static double distance(Point const &a, const Point &b);
	Cart  get_cart() const { return _cart; }
	Polar getPolar() const { return _polar; }

	void toPolarCoor();
	void toCartesianCoor();
	void show_cartesian();
	void show_polar();

	// double closest_pair_points(double *arr) - in process
	
};