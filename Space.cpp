#include "Space.h"
#include <iostream>
#include <cmath>

double Space::dsitance(Space const &a, Space const &b)
{
	return sqrt(
		   pow(b.get_cart().x - a.get_cart().x, 2) 
		 + pow(b.get_cart().y - a.get_cart().y, 2)
		 + pow(b._z - a._z, 2));
}